<?php

class Comments extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT * FROM `comments`')) {

            $comments = $gry->fetch_all(MYSQLI_ASSOC);

            return $comments;
        }
    }

    public function getByProductId($id)
    {
        if ($gry = $this->connect->query('SELECT c.`id` as `id`, `comment`, `estimate`, u.`login` as customer_name FROM `comments` c
        JOIN `users` u ON u.id = c.customer_id
        WHERE `products_id` = ' . $id)) {
            $comments = $gry->fetch_all(MYSQLI_ASSOC);
            return $comments;
        }
    }

    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `comments` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;
                if ($this->connect->query('UPDATE `comments` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds; 
                $values = $data->values; 
                if ($this->connect->query('INSERT INTO `comments` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type' && $key != 'whom_add_name') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

            case 'edit':
                $data = (object) null;
                $set = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type' && $key != 'whom_add_name') {
                        if ($set == '') {
                            $set = '`' . $key . '`="' . $value . '"';
                        } else {
                            $set = $set . ', `' . $key . '`="' . $value . '"';
                        }
                    }
                }

                $data->id = $array['id'];
                $data->query_type = 'upd';
                $data->set = $set;

                return $data;
                break;
        }
    }
}
