<?php

class Products extends DbConnect
{

    public function usconnect()
    {
        if ($gry = $this->connect->query('SELECT p.`id` as id, p.`name` as name, p.`img_url` as img_url, p.`average_price` as average_price, 
        p.`date` as date, p.`whom_add_id` as whom_add_id, u.`login` as whom_add_name,
        IFNULL((SELECT count(c.`id`) FROM `comments` c WHERE c.`products_id` = p.`id`), 0) as review_cnt
        FROM `products` p
        JOIN `users` u ON u.`id` = p.`whom_add_id`
        ')) {

            $products = $gry->fetch_all(MYSQLI_ASSOC);

            return $products;
        }
    }

    public function getById($id)
    {
        if ($gry = $this->connect->query('SELECT p.`id` as id, p.`name` as name, p.`img_url` as img_url, p.`average_price` as average_price, 
        p.`date` as date, p.`whom_add_id` as whom_add_id, u.`login` as whom_add_name,
        IFNULL((SELECT count(cr.`id`) FROM `comments` cr WHERE cr.`products_id` = p.`id`), 0) as review_cnt,
        IFNULL((SELECT sum(c.`estimate`) FROM `comments` c WHERE c.`products_id` = p.`id`), 0) as estimate_sum    
        FROM `products` p
        JOIN `users` u ON u.`id` = p.`whom_add_id` 
        WHERE p.`id` = ' . $id)) {
            $product = $gry->fetch_array(MYSQLI_ASSOC);
            return $product;
        }
    }

    public function save($data)
    {

        switch ($data->query_type) {

            case 'del':
                $id = $data->id;
                if ($this->connect->query('DELETE FROM `products` WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'upd':
                $id = $data->id;
                $set = $data->set;
                if ($this->connect->query('UPDATE `products` SET ' . $set . ' WHERE `id` = ' . $id)) {
                    return true;
                } else {
                    return false;
                }
                break;

            case 'ins':
                $filds = $data->filds;
                $values = $data->values;
                if ($this->connect->query('INSERT INTO `products` (' . $filds . ') VALUES (' . $values . ')')) {
                    return true;
                } else {
                    return false;
                }
                break;
        }
    }

    public function before_save($array)
    {

        switch ($array['query_type']) {

            case 'add':
                $data = (object) null;
                $filds = '';
                $values = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type' && $key != 'whom_add_name' && $key != 'date_show') {
                        if ($filds == '') {
                            $filds = '`' . $key . '`';
                        } else {
                            $filds = $filds . ', `' . $key . '`';
                        }
                        if ($values == '') {
                            $values = '"' . $value . '"';
                        } else {
                            $values = $values . ', "' . $value . '"';
                        }
                    }
                }

                $data->query_type = 'ins';
                $data->filds = $filds;
                $data->values = $values;

                return $data;
                break;

            case 'edit':
                $data = (object) null;
                $set = '';

                foreach ($array as $key => $value) {
                    if ($key != 'id' && $key != 'query_type' && $key != 'whom_add_name' && $key != 'date_show') {
                        if ($set == '') {
                            $set = '`' . $key . '`="' . $value . '"';
                        } else {
                            $set = $set . ', `' . $key . '`="' . $value . '"';
                        }
                    }
                }

                $data->id = $array['id'];
                $data->query_type = 'upd';
                $data->set = $set;

                return $data;
                break;
        }
    }
}
