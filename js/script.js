function getXHR(url, dest = '#', async = true, callback = null) {
    var request = new XMLHttpRequest();
    request.open('GET', url, async);
    request.send(null);
    request.onload = function() {
        if (request.status == 200) {
            if (dest != '#') window.location = dest;
            if (!!callback) {
                callback(request.response);
            }
        } else {
            alert('Error ' + request.status + ': ' + request.statusText + ' Response: ' + request.response);
        }
    };
}

function showUserForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('users')[0].style.display = "none";
    } else {
        data = getXHR(
            '/users?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('login').value = data.login;
                document.getElementById('password').value = data.password;
                document.getElementById('email').value = data.email;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('users')[0].style.display = "none";
    }
    document.getElementsByClassName('users_modal')[0].style.display = "block";
}

function showMainForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('main')[0].style.display = "none";
    } else {
        data = getXHR(
            '/main?sid=' + id,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                document.getElementById('name').value = data.name;
                document.getElementById('img_url').value = data.img_url;
                document.getElementById('average_price').value = data.average_price;
                document.getElementById('date_show').value = data.date.substr(0, 10);
                document.getElementById('whom_add_name').value = data.whom_add_name;
            }
        );
        document.getElementById('query_type').value = "edit";
        document.getElementById('id').value = id;
        document.getElementById('id_disabled').value = id;
        document.getElementsByClassName('main')[0].style.display = "none";
    }
    document.getElementsByClassName('main_modal')[0].style.display = "block";
}

function showCommentForm(id = 0) {
    if (id == 0) {
        document.getElementById('query_type').value = "add";
        document.getElementsByClassName('product')[0].style.display = "none";
    }
    document.getElementsByClassName('product_modal')[0].style.display = "block";
}

function eyePass() {
    if (document.getElementById('eye_pass').checked == true) {
        document.getElementById('eye_pass').title = 'Hide password';
        document.getElementById('password').type = 'text';
    } else {
        document.getElementById('eye_pass').title = 'Show password';
        document.getElementById('password').type = 'password';
    }
}

function ExistName(form, field = 0, id = 0, query_type = 0) {
    let base = form.baseURI.split('/');
    if (form[query_type].value == 'edit') {
        form.submit();
    } else {
        let name = field == 0 ? 'name' : form[field].name;
        data = getXHR(
            '/' + base[base.length - 1] + '?' + name + '=' + form[field].value,
            dest = '#',
            true,
            response => {
                let data = JSON.parse(response);
                let = errorMessage = field == 0 ? 'ERROR: "Form Data Lost - Try Again"' : 'ERROR: "' + form[field].value + ' Already Exists - Enter Another"';
                if (data == 'YES') {
                    document.getElementById(name).value = errorMessage;
                } else {
                    form.submit();
                }
            }
        );
    }
}

function closeForm(classname) {
    document.getElementsByClassName(classname + '_modal')[0].style.display = "none";
    document.getElementsByClassName(classname)[0].style.display = "block";
}

function clearForm() {
    let input = document.querySelectorAll('input');
    input.forEach(e => {
        if (e.value != 'E n t e r')
            e.value = ''
    });
    let inputnumber = document.querySelectorAll('input[name^=id]');
    inputnumber.forEach(e => e.value = 0);
    let inputwhomadd = document.querySelectorAll('input[name^=whom_add_name]');
    inputwhomadd.forEach(e => e.value = document.getElementById('loginname').getAttribute('name'));
    let estimate = document.querySelectorAll('select[name^=estimate]');
    estimate.forEach(e => e.value = 0);
}