<?php
session_start();
include 'autoload.php';

Route::add('/',function(){
    include('main.php');
});

Route::add('/main',function(){
    include('main.php');
});

Route::add('/login',function(){
    include('templ/login.php');
});

Route::add('/products',function(){
    include('products.php');
});

Route::add('/logout',function(){
    include('logout.php');
});

Route::add('/userProfile',function(){
    include('userProfile.php');
});

Route::add('/users',function(){
    include('users.php');
});

Route::pathNotFound(function($param){
    include('templ/404.php');
});


Route::methodNotAllowed(function(){
    include('templ/denied.php');
});

Route::add('/dataError',function(){
    include('templ/dataError.php');
});


Route::run('/');