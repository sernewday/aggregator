<head>
    <meta charset="utf-8">
    <title>Ser Aggregator - Main</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="main_form" class="main_modal">
        <a href="javascript: clearForm(); closeForm('main')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>Products Card</h1>
        </a>
        <form action="main.php" onsubmit="ExistName(this, 1, 6, 8);return false;" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0" required>

            <label for="img_url">
                Name >
            </label>
            <input type="text" name="name" placeholder="Like [a-Z,а-Я,_,0-9]" id="name" required pattern="<?= REG_NAME ?>" title="Like [a-Z,а-Я,_,0-9] not less 3">
            <label for="img_url">
                Url Photo>
            </label>
            <input type="text" name="img_url" placeholder="Like Http// Or /upload/img/imgname.jpg" id="img_url" required title="Like Http// Or /upload/img/imgname.jpg">
            <label for="average_price">
                Average Price >
            </label>
            <input type="text" name="average_price" placeholder="Like 12345.67, Not negative" id="average_price" required pattern="<?= REG_DECIMAL ?>" title="Like 12345.67, Not negative">
            <label for="date_show">
                Date Add >
            </label>
            <input disabled="disabled" type="date" name="date_show" placeholder="Show And Save Auto" id="date_show" title="Show And Save Auto">
            <label for="whom_add_name">
                Whom Add >
            </label>
            <input disabled="disabled" type="text" name="whom_add_name" id="whom_add_name" value="<?= $_SESSION['login'] ?>" title="Show And Save Auto">
            
            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="whom_add_id" name="whom_add_id" value="<?= $_SESSION['id'] ?>">            
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div id="loginname" name="<?= $_SESSION['login'] ?>"></div>
    <div class="main">
        <table class="main">
            <tbody>
                <tr>
                    <th class="main-th">ID</th>
                    <th class="main-th">Name</th>
                    <th class="main-th">Photo</th>
                    <th class="main-th">Average Price</th>
                    <th class="main-th">Date Add</th>
                    <th class="main-th">Whom Add</th>
                    <th class="main-th">Revew Count</th>
                    <th class="main-th img-icon">
                        <a href="javascript: showMainForm();"><img class="img-logo" src="images/add.png" title="Add"></a>
                        <a class="a-icon" href="#"><img class="img-logo" src="images/edit.png" title="Edit"></a>
                        <a class="a-icon" href="#"><img class="img-logo" src="images/trash.png" title="Trash"></a>
                    </th>
                </tr>
                <?php foreach ($rows as $row) {
                    echo '<tr><td class="main-td1">' . $row['id'] . '</td>';
                    echo  '<td class="main-td"><a href="/products?id=' . $row['id'] . '">' . $row['name'] . '</a></td>';
                    echo  '<td class="main-td-foto"><a href="' . $row['img_url'] . '"><img class="img-mini" src="' . $row['img_url'] . '"></td>';
                    echo  '<td class="main-td">' . $row['average_price'] . '</td>';
                    echo  '<td class="main-td">' . $row['date'] . '</td>';
                    echo  '<td class="main-td">' . $row['whom_add_name'] . '</td>';
                    echo  '<td class="main-td">' . $row['review_cnt'] . '</td>';
                    echo  '<td class="main-td2 img-icon">';
                    echo '<a href="javascript: showMainForm();"><img class="img-logo" src="images/add.png" title="Add"></a>';
                    echo '<a class="a-icon" href="javascript: showMainForm(' . $row['id'] . ');"><img class="img-logo" src="images/edit.png" title="Edit"></a>';
                    echo '<a class="a-icon" href="javascript: alert(`YOU DELETED RECORD ' . $row['id'] . '`); getXHR(`/main?did=' . $row['id'] . '`,`/main`);"><img class="img-logo" src="images/trash.png" title="Trash"></a></td></tr>';
                }
                ?>
                <tr>
                    <th class="main-th">
                        << pre</th>
                    <th class="main-th" colspan="6">page 1 of <?= $count ?>
                    </th>
                    <th class="main-th">next >></th>
                </tr>
            </tbody>
        </table>
    </div>

</body>