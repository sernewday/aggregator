<?php
session_start();
include 'autoload.php';
$currentUrl = '/comments'; 
if (!isset($_SESSION['logon'])) {
	header( 'Location: /login', true, 303 ); 
    die;
    }

$Comments = new Comments();

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $Comments->before_save($_POST); 
    if ($Comments->save($data)) {
        header( 'Location: /products?id='.$_POST['products_id'], true, 303 );
    } else {
        header( 'Location: /dataError?info=Record not insert!', true, 303 ); 
    }
}

