<?php
session_start();
$currentUrl = '/';

if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Ser Aggregator - Main</title>
    <meta name="description" content="Ser Aggregator">
    <meta name="author" content="Serhii Aksonov">
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>

</body>

</html>