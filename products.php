<?php
session_start();
include 'autoload.php';
$currentUrl = '/products';
if (!isset($_SESSION['logon'])) {
    header('Location: /login', true, 303);
    die;
}

$isExist = new IsExistsValidator();

$Products = new Products();
$Comments = new Comments();

if (!empty($_GET['id'])) {
    $product = $Products->getById($_GET['id']);
    if ($product['review_cnt'] > 0) {
        $product['average_estimate'] = $product['estimate_sum']/$product['review_cnt'];
    } else {
        $product['average_estimate'] = 0;
    }
    $rows = $Comments->getByProductId($_GET['id']);
}

include('templ/products/index.php');
