<head>
    <meta charset="utf-8">
    <title>Ser Aggregator - Product</title>
</head>

<body>
    <?php
    include "templ/header.php";
    include "templ/menu.php";
    ?>
    <div id="product_form" class="product_modal">
        <a href="javascript: clearForm(); closeForm('product')" title="close">
            <img class="close" align="right" src="/images/close-white.png">
            <h1>New Comment</h1>
        </a>
        <form action="comments.php" method="post">
            <label for="id_disabled">
                Id >
            </label>
            <input disabled="disabled" type="text" name="id_disabled" placeholder="ID" id="id_disabled" value="0" required>

            <label for="comment">
                Comment >
            </label>
            <input type="text" name="comment" placeholder="Like [a-Z,а-Я,_,0-9]" id="comment" required pattern="<?= REG_NAME ?>" title="Like [a-Z,а-Я,_,0-9] not less 3">
            <label for="whom_add_name">
                Author >
            </label>
            <input type="text" name="whom_add_name" id="whom_add_name" value="<?= $_SESSION['login'] ?>" title="Show And Save Auto">
            <label for="estimate">
                Estimate >
            </label>
            <select id="estimate" name="estimate" required">
                <option id="1" value="1">1</option>';
                <option id="2" value="2">2</option>';
                <option id="3" value="3">3</option>';
                <option id="4" value="4">4</option>';
                <option id="5" value="5">5</option>';
                <option id="6" value="6">6</option>';
                <option id="7" value="7">7</option>';
                <option id="8" value="8">8</option>';
                <option id="9" value="9">9</option>';
                <option id="10" value="10">10</option>';
            </select>

            <input type="hidden" id="id" name="id" value="0">
            <input type="hidden" id="customer_id" name="customer_id" value="<?= $_SESSION['id'] ?>">
            <input type="hidden" id="products_id" name="products_id" value="<?= $product['id'] ?>">
            <input type="hidden" id="query_type" name="query_type" value="">
            <input type="submit" value="E n t e r">
        </form>
    </div>
    <div id="loginname" name="<?= $_SESSION['login'] ?>"></div>
    <div class="product">
        <table class="product">
            <tbody>
                <?php
                echo  '<tr><td class="product-td-foto" rowspan="6"><img class="img-mini" src="' . $product['img_url'] . '"></td>';
                echo  '<td class="product-td" colspan="2">Product Name: ' . $product['name'] . '</a></td>';
                echo  '<td class="product-td-foto" rowspan="6"><img class="img-mini" src="' . $product['img_url'] . '"></td></tr>';
                echo  '<tr><td class="product-td" colspan="2">Average Price: ' . $product['average_price'] . '</td></tr>';
                echo  '<tr><td class="product-td" colspan="2">Date Add: ' . $product['date'] . '</td></tr>';
                echo  '<tr><td class="product-td" colspan="2">Whom Add: ' . $product['whom_add_name'] . '</td></tr>';
                echo  '<tr><td class="product-td" colspan="2">Comments: ' . $product['review_cnt'] . '</td></tr>';
                echo  '<tr><td class="product-td" colspan="2">Average Estimate: ' . $product['average_estimate'] . '</td></tr>';
                echo  '<tr><td class="comments-th1" colspan="4">All Comments</td></tr>';
                echo  '<tr><td class="comments-th2" >Add Comment <a href="javascript: showCommentForm();"><img class="img-logo" src="images/add.png" title="Add"></a></td>';
                echo  '<td class="comments-th2">Comment</td>';
                echo  '<td class="comments-th2">Author</td>';
                echo  '<td class="comments-th2">Estimate</td>';
                foreach ($rows as $row) {
                    echo '<tr><td class="comments-td">' . $row['id'] . '</td>';
                    echo  '<td class="comments-td">' . $row['comment'] . '</td>';
                    echo  '<td class="comments-td">' . $row['customer_name'] . '</td>';
                    echo  '<td class="comments-td">' . $row['estimate'] . '</td></tr>';
                }
                ?>
            </tbody>
        </table>
</body>