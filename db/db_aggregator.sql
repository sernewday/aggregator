-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.19 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5958
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_aggregator
CREATE DATABASE IF NOT EXISTS `db_aggregator` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `db_aggregator`;

-- Dumping structure for table db_aggregator.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `products_id` varchar(50) NOT NULL,
  `estimate` int NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `customer_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table db_aggregator.comments: ~4 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
REPLACE INTO `comments` (`id`, `products_id`, `estimate`, `comment`, `customer_id`) VALUES
	(1, '3', 4, 'good', 3),
	(2, '3', 5, 'great', 3),
	(3, '3', 4, 'good', 3),
	(4, '3', 3, 'nice', 3),
	(6, '3', 1, 'Like', 3),
	(7, '5', 5, 'Look alike happy women', 3),
	(9, '5', 9, 'The best', 9),
	(10, '1', 7, 'Fresh Idia', 9),
	(11, '3', 7, 'Belle', 1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table db_aggregator.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/upload/img/product_default.jpg',
  `average_price` decimal(7,2) DEFAULT '0.00',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `whom_add_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table db_aggregator.products: ~6 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`id`, `name`, `img_url`, `average_price`, `date`, `whom_add_id`) VALUES
	(1, 'Red Dress', '/upload/img/product_default.jpg', 75.00, '2021-03-31 19:00:19', 3),
	(2, 'Black Dress', 'https://image.freepik.com/free-photo/attractive-woman-bright-makeup-black-dresses-fashion-modern-style_163305-76776.jpg', 90.00, '2021-03-31 20:59:34', 3),
	(3, 'Shiny  Dress', 'https://image.freepik.com/free-photo/woman-in-sequin-dress_144627-28196.jpg', 105.00, '2021-03-31 21:32:21', 1),
	(5, 'Two Dresses For The Price Of One ', 'https://image.freepik.com/free-photo/two-beautiful-women-in-evening-dresses-posing-holding-champaign-glass_176420-3763.jpg', 88.00, '2021-03-31 21:06:50', 3),
	(6, 'Grey Jacket ', 'https://image.freepik.com/free-photo/portrait-of-young-beautiful-business-woman-blonde-in-black-dress-jacket-and-with-bag-on-gray-background_114963-4173.jpg', 45.50, '2021-03-31 21:10:16', 1),
	(8, 'Short Red Dress', 'https://image.freepik.com/free-photo/portrait-of-fashion-cute-young-woman-model-in-a-red-dress-on-a-white-wall_158538-8045.jpg', 74.15, '2021-04-01 13:02:38', 3);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table db_aggregator.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table db_aggregator.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `login`, `password`, `email`) VALUES
	(1, 'test44', 'Test_444', 'test@test.com'),
	(2, 'Wild44', 'Wild_444', 'willy@gmail.com'),
	(3, 'Cowboy5', 'Cowboy_5', 'sernewday@gmail.com'),
	(4, 'Royals', 'Royal_44', 'roy@green.com'),
	(5, 'Glebov', 'Glebov', 'gleb@green.com'),
	(6, 'Timaty', 'Timaty_6', 'tyni@tyn.com'),
	(7, 'Superadmin1', 'Superadmin_1', 'admin@admin.com'),
	(8, 'New_rosty', 'New_rost25', 'test@test.com'),
	(9, 'PolForost', 'pol_24REst', 'noanswer@polforost.pl');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
