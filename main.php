<?php
session_start();
include 'autoload.php';
$currentUrl = '/main'; 
if (!isset($_SESSION['logon'])) {
	header( 'Location: /login', true, 303 ); 
    die;
    }

$isExist = new IsExistsValidator();

$Main = new Products();
$rows = $Main->usconnect();
$count = round(count($rows)/7 + 0.45);


if (!empty($_GET['did'])) {
    $data = (object) null;
    $data->id = $_GET['did'];
    $data->query_type = 'del';
    if ($Main->save($data)) {
        echo 'OK'; die;
    } else {
        header( 'Location: /dataError?info=Record not deleted!', true, 303 );  
    }
}

if (!empty($_GET['sid'])) {
    if ($data = $Main->getById($_GET['sid'])) {
        echo json_encode($data); die;
    } else {
        header( 'Location: /dataError?info=Select do not received!', true, 303 );  
    }
}

if (!empty($_GET['name'])) {
    $data = $isExist->validate($_GET['name'], 'products', 'name');
    if ($data) {
        echo json_encode($data);
        die;
    } else {
        header('Location: /dataError?info=Select do not received!', true, 303);
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'add') {
    $data = $Main->before_save($_POST); 
    if ($Main->save($data)) {
        header( 'Location: /main', true, 303 );
    } else {
        header( 'Location: /dataError?info=Record not insert!', true, 303 ); 
    }
}

if (!empty($_POST['query_type']) && $_POST['query_type'] == 'edit') {
    $data = $Main->before_save($_POST); 
    if ($Main->save($data)) {
        header( 'Location: /main', true, 303 );
    } else {
        header( 'Location: /dataError?info=Record not update!', true, 303 ); 
    }
}
include('templ/main/index.php');
