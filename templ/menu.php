<div class="menu">
<nav>
<a <?php echo ($currentUrl == '/' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/">| Main |</a>
<a <?php echo ($currentUrl == '/userProfile' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/userProfile">| Profile |</a>
<a <?php echo ($currentUrl == '/users' ? 'class="a-menu-active"' : 'class="a-menu"'); ?> href="/users">| Users |</a>
<a class="a-menu" href="/logout">| Logout <span class="color_span_menu"><?= ' '.$_SESSION['login'].' ' ?></span> |</a>
</nav>
</div>
