<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Ser Aggregator - Error 404</title>
    <meta name="description" content="Ser Aggregator">
    <meta name="author" content="Serhii Aksonov">
</head>

<body>
    <?php 
        include "templ/header.php";
        include "templ/menu.php"; 
    ?>
    <div class="page_404">
        Page "<?= $param  ?>" not found!
    </div>
</body>

</html>